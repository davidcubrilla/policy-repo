import React from 'react';

export class PolicyCard extends React.Component {

    deletePolicy(policy) {
        let id = policy.policyId;
        let deleteURL = `http://localhost:8080/policies/${id}`
        fetch(deleteURL, {method: 'DELETE'})
        .then(()=>{
            this.props.unShow();
        })
    }

    render() {
        let policy = this.props.pol;
        return (
            <section className="Policy-card-div">
                <button className="deleteButton" onClick={ () => {this.deletePolicy(policy)} }>Remove {policy.policyType} Policy</button>
                <div className='infoContainer'>
                    <div className='info'>
                        <h5>Coverage: </h5>
                        <h4>{policy.policyType}</h4>
                    </div>
                    <div className='info'>
                        <h5>Added Date: </h5>
                        <h4>{policy.addedDate}</h4>
                    </div>
                    <div className='info'>
                        <h5>Renewal Date: </h5>
                        <h4>{policy.renewalDate}</h4>
                    </div>
                    <div className='info'>
                        <h5>Monthly Premium: </h5>
                        <h4>${policy.monthlyPremium}</h4>
                    </div>
                </div>
            </section>
        )
    }
}