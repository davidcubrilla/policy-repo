import React from 'react';
import {CustomerCard} from "./CustomerCard.js"
import { AddCustomerForm } from './AddCustomerForm.js';

import "../App.css";

export class SearchSection extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            customer: '',
            customerToRender: 'All',
            customers: [],
            isLoaded: false,
            error: null,
            renderAddCustomerForm: false,
        }

        this.handleChange = this.handleChange.bind(this)

        //have to bind this method to parent so that it can be called from child
        this.gettingAllCustomers = this.gettingAllCustomers.bind(this)
    }

    //handle the changes on the search bar so that it is a controlled component 
    //and it allows the user to type onto the search bar aka 'input'
    handleChange(event) {
        this.setState( {customer: event.target.value} )
    }

    
    gettingAllCustomers(deleteCustomer) {
        this.setState( {renderAddCustomerForm: false} ); //need to make sure no form is rendered

        //this is for when it is only viewing one customer
        //so that when it is deleted then it goes back to viewing all customers
        let deletingCustomer = deleteCustomer;
        if (deletingCustomer) {
            this.setState( {customerToRender: 'All'} )
        };
        /////
        
        let userURL = `http://localhost:8080/customers`
        // use the ES6 fetch syntax
        fetch(userURL)
        .then(response => response.json()) //always do this for JSON data
        .then(
            // handle the result
            (results) => {
                console.log(results);
                // take the returned 'results' array and use it in our state
                this.setState( {customers: results, isLoaded:true } )
            },
            // or handle the error
            (errors) => {
                this.setState( {isLoaded: true, error: errors} );
            }
        )
    }

    componentDidMount() {
        this.gettingAllCustomers()
    }

    customerSearch(customerName) {
        for (let i = 0; i < this.state.customers.length; i++) {
            let name = this.state.customers[i].firstName + " " + this.state.customers[i].lastName;
            if (name.includes(customerName)) {
                this.setState( {customerToRender: this.state.customers[i]} )
            }
        }
        
    }

    render() {  
        if(this.state.customerToRender === 'All') {
            return(
                <section>
                    <header className='head'>
                        <h1 className="logo">Allstate</h1>
                        <input className="searchBar" placeholder="Customer's name" type='text' name='customer' value={this.state.customer} onChange={this.handleChange} />
                        <button className="searchButton" onClick={ () => {this.customerSearch(this.state.customer)} }>Search</button>
                    </header>
            
                    <section className="addCustomer">
                        {this.state.renderAddCustomerForm ? 
                            //the AddCustomerForm is getting the gettingAllCustomer method because it needs it to change the state of the parent 'SearchSection'
                            //so that that the page can rerender when a customer is deleted, added, or updated
                            <AddCustomerForm get={this.gettingAllCustomers} />
                            : 
                            <button className="addButton" onClick={ () => {this.setState({renderAddCustomerForm: true})} }>Add a customer</button>
                        }
                    </section>
    

                    {this.state.customers.map( cus => (
                        //the CustomerCard(s) is getting the gettingAllCustomer method because it needs it to change the state of the parent 'SearchSection'
                        //so that that the page can rerender when a customer is deleted, added, or updated
                        <CustomerCard key={cus.firstName+cus.lastName} c={cus} get={this.gettingAllCustomers}/>
                    ))}
                </section> 
            )
        } else {
            return(
                <section>
                     <header className='head'>
                        <h1 className="logo">Allstate</h1>
                        <input className="searchBar" placeholder="Customer's name" type='text' name='customer' value={this.state.customer} onChange={this.handleChange} />
                        <button className="searchButton" onClick={ () => {this.customerSearch(this.state.customer)} }>Search</button>
                        <button className="viewAllButton" onClick={ () => {this.setState( {customerToRender: 'All', customer: ''} )} }>View All</button>
                    </header>
                    
                   
                    <CustomerCard c={this.state.customerToRender} get={this.gettingAllCustomers}/> 
                </section> 
            )
        }
        
    }
}