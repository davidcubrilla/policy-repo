import React from 'react';
import {PolicyCard} from "./PolicyCard.js"
import { AddPolicyForm } from './AddPolicyForm.js';

export class CustomerCard extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            renderAddPolicyForm : false
        }

        this.unShowForm = this.unShowForm.bind(this)
        this.unShowPolicyCard = this.unShowPolicyCard.bind(this)
    }
   
    // need to delete policies first to delete customers because of how backend is setup
    async deletePolicies(customerPolicies) {
        for(let i = 0; i < customerPolicies.length; i++) {
            let id = customerPolicies[i].policyId;
            let deleteURL = `http://localhost:8080/policies/${id}`
            fetch(deleteURL, {method: 'DELETE'})
            .then(response => response.text())
        }
    }

    async deleteCustomer(customer) {
        //have to delete customers policies first then delete customer
        await this.deletePolicies(customer.policies);
        let deleteURL = `http://localhost:8080/customers/${customer.customerId}`
        fetch(deleteURL, {method: 'DELETE'})
        .then(()=>{
            //this calls the getAllCustomer method from searchSection so that the page can rerender and stay of updated
            this.props.get(true);
        })
    }

    unShowForm() {
        this.setState({
            renderAddPolicyForm: false
        })
        //and so that it can render the new policy as well
        this.props.get(false);
    }

    unShowPolicyCard() {
        //and so that it can render the page without the deleted policy
        this.props.get(false);
    }

    render() {
        let customer = this.props.c;
        let policies = this.props.c.policies;
        return (
            <section className="customerCard">
                <div className='infoContainer'>
                    <button className="deleteButton" onClick={ () => {this.deleteCustomer(customer)} }>Remove {customer.firstName} {customer.lastName}</button>
                    <div className='info'>
                        <h5>Customer Id: </h5>
                        <h4>{customer.customerId}</h4>
                    </div>
                    <div className='info'>
                        <h5>First Name: </h5>
                        <h4>{customer.firstName}</h4>
                    </div>
                    <div className='info'>
                        <h5>Last Name: </h5>
                        <h4>{customer.lastName}</h4>
                    </div>
                    <div className='info'>
                        <h5>City: </h5>
                        <h4>{customer.city}</h4>
                    </div>
                    <div className='info'>
                        <h5>Zip Code: </h5>
                        <h4>{customer.zipCode}</h4>
                    </div>
                    <div className='info'>
                        <h5>Date Joined: </h5>
                        <h4>{customer.dateJoined}</h4>
                    </div>
                </div>

                <section className="policySection">
                    {policies.map( p => (
                        <PolicyCard key={p.policyId} pol={p} unShow={this.unShowPolicyCard}/>            
                    ) )}

                    {this.state.renderAddPolicyForm ? 
                        <AddPolicyForm c={customer} unShow={this.unShowForm}/>
                        : 
                        <button className="addButton" onClick={ () => {this.setState({renderAddPolicyForm: true})} }>Add a Policy</button>
                    }
                </section>
               
            </section>
        )
    }
}