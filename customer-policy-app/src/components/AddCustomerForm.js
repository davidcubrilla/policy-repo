import React from 'react';

export class AddCustomerForm extends React.Component {
    constructor(props) {
        super(props) 
        this.state = {
            firstName: '',
            lastName: '',
            city: '',
            zipCode: '',
            date: new Date().toISOString().slice(0, 10)
        }

        this.handleFirstNameChange = this.handleFirstNameChange.bind(this)
        this.handleLastNameChange = this.handleLastNameChange.bind(this)
        this.handleCityChange = this.handleCityChange.bind(this)
        this.handleZipCodeChange = this.handleZipCodeChange.bind(this)
    }

    //input handlers
    handleFirstNameChange(event) {
        this.setState( {firstName: event.target.value} )
    }
    handleLastNameChange(event) {
        this.setState( {lastName: event.target.value} )
    }
    handleCityChange(event) {
        this.setState( {city: event.target.value} )
    }
    handleZipCodeChange(event) {
        this.setState( {zipCode: event.target.value} )
    }

    addCustomer() {
        let customerURL = `http://localhost:8080/customers`
        let requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                city: this.state.city,
                zipCode: this.state.zipCode,
                dateJoined: this.state.date
            })
        };
    
        fetch(customerURL, requestOptions)
        .then(()=>{
            //need to update page so that the form can exit
            this.props.get(false);
        })
    }

    render () {
        return(
            <section>
                <h1>New Customer</h1>
                {/* 
                today's date */}
                <input type="text" name='first' placeholder="First Name" value={this.state.firstName} onChange={this.handleFirstNameChange} />
                <br />
                <input type="text" name='last' placeholder="Last Name" value={this.state.lastName} onChange={this.handleLastNameChange} />
                <br />
                <input type="text" name='city' placeholder="City" value={this.state.city} onChange={this.handleCityChange} />
                <br />
                <input type="text" name='zipCode' placeholder="Zipcode" value={this.state.zipCode} onChange={this.handleZipCodeChange} />
                <br />
                <button className="addButton submitButton" onClick={ () => {this.addCustomer()} }>Submit</button>
                <button className="cancelButton" onClick={ () => {this.props.get(false)} }>Cancel</button>
            </section>
        )
    }
}