import React from 'react';

export class AddPolicyForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            policyType: 'Collision',
            monthlyPremium: '',
            addedDate: new Date().toISOString().slice(0, 10),
            renewalDate: '',
        }

        this.handleSelect = this.handleSelect.bind(this)
        this.handleMonthlyPremiumChange = this.handleMonthlyPremiumChange.bind(this)
        this.handlerenewalDateChange = this.handlerenewalDateChange.bind(this)
    }

    //handlers
    handleSelect(event) {
        this.setState( {policyType: event.target.value} )
    }

    handleMonthlyPremiumChange(event) {
        this.setState( {monthlyPremium: event.target.value} )
    }

    handlerenewalDateChange(event) {
        this.setState( {renewalDate: event.target.value} )
    }

    addPolicy() {
        let policyURL = `http://localhost:8080/policies`
        let requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 
                policyType: this.state.policyType,
                monthlyPremium: this.state.monthlyPremium,
                addedDate: this.state.addedDate,
                renewalDate: this.state.renewalDate,
                customerId: this.props.c.customerId
            })
        };
    
        fetch(policyURL, requestOptions)
        .then(()=>{
            //so that the form component will leave screen
            this.props.unShow();
        })
    }

    render() {
        return(
            <section>
                <h1>New Policy</h1>
                <select onChange={this.handleSelect}>
                    <option value='collision'>Collision</option>
                    <option value='liability'>Liability</option>
                    <option value='medical'>Medical</option>
                    <option value='roadside'>Roadside</option>
                    <option value='personal injury'>Personal Injury</option>
                    <option value='personal umbrella'>Personal Umbrella</option>
                    <option value='rental reimbursement'>Rental Reimbursement</option>
                </select>
                <br />
                <input type="text" name='monthlyPremium' placeholder="Monthly Premium" value={this.state.monthlyPremium} onChange={this.handleMonthlyPremiumChange} />
                <br />
                <input type="text" name='renewalDate' placeholder="Renewal Date" value={this.state.renewalDate} onChange={this.handlerenewalDateChange} />
                <br />
                <button className="addButton submitButton" onClick={ () => {this.addPolicy()} }>Submit</button>
                <button className="cancelButton" onClick={ () => {this.props.unShow()} }>Cancel</button>
            </section>
        )
    }

    
}