import React from "react";
import "./App.css";
import {SearchSection} from './components/SearchSection.js';

function App() {
  return (
    <div className="App">
      <SearchSection />
    </div>
  );
}

export default App;
